package br.senai.sp.informatica.associacao.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.associacao.model.Agenda;
import br.senai.sp.informatica.associacao.repo.AgendaRepo;

@Service
public class AgendaService {
	@Autowired
	private AgendaRepo repo;
	
	public void salvar(Agenda obj) {
		repo.save(obj);
	}
	
	public List<Agenda> getAgendas() {
		return repo.findAll().stream()
				.filter(obj -> !obj.isDesativado())
				.collect(Collectors.toList());
	}

	public Agenda getAgenda(int id) {
		return repo.findById(id).orElse(null);
	}
	
	private boolean delOk;
	
	public boolean removeAgenda(int[] lista) {
		delOk = true;
		
		Arrays.stream(lista).forEach(id -> {
			Agenda obj = getAgenda(id);
			if(obj != null) {
				obj.setDesativado(true);
				repo.save(obj);
			} else {
				delOk = false;
			}
		});
		
		return delOk;
	}
}
