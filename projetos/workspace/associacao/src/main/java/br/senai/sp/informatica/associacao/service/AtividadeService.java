package br.senai.sp.informatica.associacao.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.associacao.model.Atividade;
import br.senai.sp.informatica.associacao.repo.AtividadeRepo;

@Service
public class AtividadeService {
	@Autowired
	private AtividadeRepo repo;
	
	public void salvar(Atividade obj) {
		repo.save(obj);
	}
	
	public List<Atividade> getAtividades() {
		return repo.findAll().stream()
				.filter(obj -> !obj.isDesativado())
				.collect(Collectors.toList());
	}

	public Atividade getAtividade(int id) {
		return repo.findById(id).orElse(null);
	}
	
	private boolean delOk;
	
	public boolean removeAtividade(int[] lista) {
		delOk = true;
		
		Arrays.stream(lista).forEach(id -> {
			Atividade obj = getAtividade(id);
			if(obj != null) {
				obj.setDesativado(true);
				repo.save(obj);
			} else {
				delOk = false;
			}
		});
		
		return delOk;
	}
}
