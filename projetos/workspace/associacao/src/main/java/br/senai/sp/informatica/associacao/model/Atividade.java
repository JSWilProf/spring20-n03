package br.senai.sp.informatica.associacao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
public class Atividade {
	@Id
	@Column(name = "idAtividade")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Size(min=3, max=150, message="O Nome deve ter entre 3 e 150 caracteres")
	private String nome;
	@Max(value=300, message="O valor não pode ser maior que R$ 300,00")
	@Min(value=0, message="O valor não pode ser negativo")
	private Double valor;
	private boolean desativado;
}
