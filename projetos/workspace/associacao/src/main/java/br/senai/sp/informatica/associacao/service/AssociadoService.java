package br.senai.sp.informatica.associacao.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.associacao.model.Associado;
import br.senai.sp.informatica.associacao.repo.AssociadoRepo;

@Service
public class AssociadoService {
	@Autowired
	private AssociadoRepo repo;
	
	public void salvar(Associado obj) {
		repo.save(obj);
	}
	
	public List<Associado> getAssociados() {
		return repo.findAll().stream()
				.filter(obj -> !obj.isDesativado())
				.collect(Collectors.toList());
	}

	public Associado getAssociado(int id) {
		return repo.findById(id).orElse(null);
	}
	
	private boolean delOk;
	
	public boolean removeAssociado(int[] lista) {
		delOk = true;
		
		Arrays.stream(lista).forEach(id -> {
			Associado obj = getAssociado(id);
			if(obj != null) {
				obj.setDesativado(true);
				repo.save(obj);
			} else {
				delOk = false;
			}
		});
		
		return delOk;
	}
}
