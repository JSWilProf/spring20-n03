package br.senai.sp.informatica.associacao.contoller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.informatica.associacao.component.JsonError;
import br.senai.sp.informatica.associacao.model.Agenda;
import br.senai.sp.informatica.associacao.model.Associado;
import br.senai.sp.informatica.associacao.model.Atividade;
import br.senai.sp.informatica.associacao.model.Horario;
import br.senai.sp.informatica.associacao.model.vo.Agendamento;
import br.senai.sp.informatica.associacao.service.AgendaService;
import br.senai.sp.informatica.associacao.service.AssociadoService;
import br.senai.sp.informatica.associacao.service.AtividadeService;

@RestController
@RequestMapping("/api/agenda")
public class AgendaController {
	@Autowired
	private AgendaService service;
	@Autowired
	private AssociadoService associadoService;
	@Autowired
	private AtividadeService atividadeService;
	
	@PostMapping("/salvar")
	public ResponseEntity<Object> salvar(@RequestBody @Valid Agendamento obj, BindingResult result) {
		if (result.hasErrors()) {
			return ResponseEntity.unprocessableEntity()
					.contentType(MediaType.APPLICATION_JSON)
					.body(JsonError.build(result));
		} else {
			Associado associado = associadoService.getAssociado(obj.getAssociadoId());
			Atividade atividade = atividadeService.getAtividade(obj.getAtividadeId());
			Horario horario = Horario.valueOf(obj.getHorario());
			Agenda agenda = Agenda.builder()
					.associado(associado)
					.atividade(atividade)
					.horario(horario).build();
			associado.getAgendamentos().add(agenda);
			service.salvar(agenda);
			associadoService.salvar(associado);
			return ResponseEntity.ok().build();
		}
	}

	@RequestMapping("/listar")
	public ResponseEntity<List<Agenda>> listar() {
		return ResponseEntity.ok(service.getAgendas());
	}

	@RequestMapping("/listar/{id}")
	public ResponseEntity<Object> listar(@PathVariable("id") int id) {
		Associado obj = associadoService.getAssociado(id);
		if(obj != null && !obj.isDesativado()) {
			return ResponseEntity.ok(obj.getAgendamentos().stream()
					.map(agenda -> agenda.getAtividade())
					.collect(Collectors.toList()));
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping("/horarios")
	public ResponseEntity<Horario[]> horarios() {
		return ResponseEntity.ok(Horario.values());
	}
	
	@PostMapping("/remover")
	public ResponseEntity<Object> remover(@RequestBody int[] lista) {
		if(service.removeAgenda(lista)) {
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.unprocessableEntity().build();
		}	
	}
	
	@RequestMapping("/editar/{id}")
	public ResponseEntity<Object> editar(@PathVariable("id") int id) {
		Agenda obj = service.getAgenda(id);
		
		if(obj != null) {
			return ResponseEntity.ok(obj);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
