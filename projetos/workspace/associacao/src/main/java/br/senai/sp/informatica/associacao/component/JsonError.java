package br.senai.sp.informatica.associacao.component;

import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;

public class JsonError {
	public static String build(BindingResult result) {
		return "{\n" 
				+ result.getFieldErrors().stream()
					.map(error -> "\"" 
						+ error.getField() 
						+ "\" : \"" 
						+ error.getDefaultMessage() 
						+ "\"")
					.collect(Collectors.joining(",\n")) 
				+ "\n}";
	}
}
