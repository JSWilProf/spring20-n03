package br.senai.sp.informatica.associacao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Agenda {
	@Id
	@Column(name="idAgenda")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	@JoinColumn(name="idAssociado")
	private Associado associado;
	@ManyToOne
	@JoinColumn(name="idAtividade")
	private Atividade atividade;
	@Enumerated(EnumType.STRING)
	private Horario horario;
	private boolean desativado;
}
