package br.senai.sp.informatica.associacao.contoller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.informatica.associacao.component.JsonError;
import br.senai.sp.informatica.associacao.model.Atividade;
import br.senai.sp.informatica.associacao.service.AtividadeService;

@RestController
@RequestMapping("/api/atividade")
public class AtividadeController {
	@Autowired
	private AtividadeService service;
	
	@PostMapping("/salvar")
	public ResponseEntity<Object> salvar(@RequestBody @Valid Atividade obj, BindingResult result) {
		if (result.hasErrors()) {
			return ResponseEntity.unprocessableEntity()
					.contentType(MediaType.APPLICATION_JSON)
					.body(JsonError.build(result));
		} else {
			service.salvar(obj);
			return ResponseEntity.ok().build();
		}
	}

	@RequestMapping("/listar")
	public ResponseEntity<List<Atividade>> listar() {
		return ResponseEntity.ok(service.getAtividades());
	}
	
	@PostMapping("/remover")
	public ResponseEntity<Object> remover(@RequestBody int[] lista) {
		if(service.removeAtividade(lista)) {
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.unprocessableEntity().build();
		}	
	}
	
	@RequestMapping("/editar/{id}")
	public ResponseEntity<Object> editar(@PathVariable("id") int id) {
		Atividade obj = service.getAtividade(id);
		
		if(obj != null) {
			return ResponseEntity.ok(obj);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
