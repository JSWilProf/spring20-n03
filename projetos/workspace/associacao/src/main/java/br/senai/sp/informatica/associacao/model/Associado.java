package br.senai.sp.informatica.associacao.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.senai.sp.informatica.associacao.model.validacao.Logradouro;
import lombok.Data;

@Data
@Entity
public class Associado {
	@Id
	@Column(name="idAssociado")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Size(min=3, max=150, message="O Nome deve ter entre 3 e 150 caracteres")
	private String nome;
	@Logradouro(max=150, message="O Endereço é inválido")
	private String endereco;
	@Max(value=120, message="A idade não pode ser maior que 120")
	@Min(value=1, message="A idade não pode ser menor que 1")
	private Integer idade;
	@Pattern(regexp="(9[0-9]{4}|[1-9][0-9]{3})-[0-9]{4}",
	 message="Nº de Telefone inválido")
	private String telefone;
	@Email
	private String email;
	@JsonIgnore
	@OneToMany(mappedBy="associado")
	private List<Agenda> agendamentos;
	private boolean desativado;
}
