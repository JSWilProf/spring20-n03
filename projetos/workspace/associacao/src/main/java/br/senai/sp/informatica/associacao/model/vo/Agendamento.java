package br.senai.sp.informatica.associacao.model.vo;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class Agendamento {
	@NotNull
	private Integer associadoId;
	@NotNull
	private Integer atividadeId;
	@NotNull
	private String horario;
}
